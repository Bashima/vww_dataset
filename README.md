# Targeted Croped and Downsampled VWW dataset #

To downsample the images from the VWW dataset,  we cropped the images to move the target object (human) in the center. 
This cropped dataset then can be downsampled to 32x32 pixel color images to fit into the MCU�s memory.

### Cite Us ###

Please cite the following paper when you use this dataset.
